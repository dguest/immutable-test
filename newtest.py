#!/usr/bin/env python3

class ConfigItem:
    """Frozen attributes class

    The idea here is to define the variables we store in an object
    _before_ that object is actually this class. For that we need to
    use __new__, rather than __init__.

    """
    def __init__(self, **args):
        fields = []
        for name, val in args.items():
            fields.append((name,val))
        fset = frozenset(fields)
        super().__setattr__("_fields", frozenset(x for x, y in fset))
        super().__setattr__("_hash", hash(fset))
        for name, val in fset:
            super().__setattr__(name, val)

    # we <3 iteration
    def __iter__(self):
        for k in self._fields:
            yield k

    # we also <3 getitem syntax
    def __getitem__(self, key):
        return getattr(self, key)

    def __contains__(self, key):
        return key in self._fields

    # and why not <3 hashing too?
    def __hash__(self):
        return self._hash
    def __eq__(self, other):
        kother = set(other)
        kself = set(self)
        if kself != kother:
            return False
        for k in kself:
            if k not in other:
                return False
            if self[k] != other[k]:
                return False
        return True

    # disable setting and deleting of attributes
    def __setattr__(self, attr, value):
        name = type(self).__name__
        raise TypeError(f"{name} doesn't support setting attributes")
    def __delattr__(self, attr):
        name = type(self).__name__
        raise TypeError(f"{name} doesn't support deleting attributes")

c = ConfigItem(x=1, y=2)
c1 = ConfigItem(y=2, x=1)
d = ConfigItem(**{k: f'{k}_val' for k in 'xyz'})
print('printing c')
for k in c:
    v = c[k]
    print(f'{k} = {v}')
print('printing d')
for k in d:
    v = d[k]
    print(f'{k} = {v}')
assert(c == c1)
assert(c != d)

try:
    c.x = 1
except TypeError as err:
    print(f"trying to set later gives: {repr(err)}")
try:
    del c.x
except TypeError as err:
    print(f"trying to delete gives: {repr(err)}")
try:
    del c['x']
except TypeError as err:
    print(f"trying to delete gives: {repr(err)}")
